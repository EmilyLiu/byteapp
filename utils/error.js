import utils from './utils.js'

// 错误提示
const tipsError = (content) => {
	uni.showModal({
		title: "温馨提示",
		content: content,
		confirmText: "我知道了",
		showCancel: false,
		success() {

		}

	})
}
// 添加成功提示
const successToast = (icon, title, url) => {
	uni.showToast({
		title: title,
		icon: icon,
		mask: true,
		duration: 2500,
		success() {
			let pages = getCurrentPages()
			if (pages.length > 1) {
				setTimeout(() => {
					uni.navigateBack()
				}, 2500)
			} else {
				if (url == "/pages/pagesPerson/index/index") {
					uni.redirectTo({
						url: "/pages/pagesPerson/index/index"
					})
				}
			}
		}
	})
}
var isPresent = false;
// 立即登录提示
const reLogin = () => {
	if (isPresent) return;
	isPresent = true;
	uni.showModal({
		title: "温馨提示",
		content: "登录后可以更好的体验销Soul",
		confirmText: "立即登录",
		success(res) {
			if (res.confirm) {
				let pages = getCurrentPages()
				let currentPage = pages[pages.length - 1]
				let path = currentPage.route
				let backurl = path == 'pages/pagesGrow/shareInfo/shareInfo' || path == 'pages/pagesPerson/vip/vip' || path ==
					'pages/pagesGrow/newExperience/newExperience' ? path : ''
				uni.navigateTo({
					url: `/pages/pagesPerson/account/account?backurl=${backurl}`
				})
				// if (backurl) {
				// 	uni.navigateTo({
				// 		url: `/pages/pagesPerson/account/account?backurl=${backurl}`
				// 	})
				// } else {
				// 	uni.reLaunch({
				// 		url: `/pages/pagesPerson/account/account?backurl=${backurl}`
				// 	})
				// }
				isPresent = false;
			}
		},
		complete() {
			isPresent = false;
		}
	})
}
// vip到期提示
const vipModal = (text) => {
	if (utils.isAndroid()) {
		uni.showModal({
			title: "温馨提示",
			content: text,
			confirmText: "立即续费",
			success(res) {
				if (res.confirm) {
					// uni.reLaunch({
					// 	url: "/pages/pagesPerson/vip/vip"
					// })
					uni.navigateTo({
						url: "/pages/pagesPerson/vip/vip"
					})
				}
			}
		})
	}else {
		uni.showModal({
			title: "温馨提示",
			content: "由于相关规范，iOS功能暂不可用",
			confirmText: "好的",
			success(res) {
				if (res.confirm) {
					// uni.reLaunch({
					// 	url: "/pages/pagesPerson/vip/vip"
					// })
					// uni.navigateTo({
					// 	url: "/pages/pagesPerson/vip/vip"
					// })
				}
			}
		})
	}
	
}
const vipOnlyModal = (text) => {
	debugger;
	if (utils.isAndroid()) {
		uni.showModal({
			title: "温馨提示",
			content: text,
			confirmText: "立即开启",
			cancelText: "稍后开启",
			success(res) {
				if (res.confirm) {
					// uni.reLaunch({
					// 	url: "/pages/pagesPerson/vip/vip"
					// })
					uni.navigateTo({
						url: "/pages/pagesPerson/vip/vip"
					})
				}
			}
		})
	}else {
		uni.showModal({
			title: "温馨提示",
			content: "由于相关规范，iOS功能暂不可用",
			confirmText: "好的",
			success(res) {
				// if (res.confirm) {
				// 	uni.navigateTo({
				// 		url: "/pages/pagesPerson/vip/vip"
				// 	})
				// }
			}
		})
	}
	
}
// 问卷调查提示
const questionModal = (text, type, confirmText,from, title, cancelText) => {
	uni.showModal({
		title: title||"温馨提示",
		content: text,
		confirmText: confirmText,
		cancelText: cancelText || "取消",
		success(res) {
			if (res.confirm) {
				uni.navigateTo({
					url: `/pages/pagesPerson/questionnaire/questionnaire?type=${type}&from=${from}`
				})
			}
		}
	})
}
// 提交答案弹窗
const submitModal = (text, type, confirmText, cancelText) => {
	uni.showModal({
		title: "温馨提示",
		content: text,
		confirmText: confirmText,
		cancelText: cancelText,
		success(res) {
			if (res.confirm) {
				if (type != "trial") {

				}
				// uni.navigateTo({
				// 	url:`/pages/pagesPerson/questionnaire/questionnaire?type=${type}`
				// })
			} else {
				if (type == "trial") {
					uni.redirectTo({
						url: "/pages/pagesPerson/index/index"
					})
				}
			}
		}
	})
}
module.exports = {
	tipsError,
	successToast,
	reLogin,
	vipModal,
	vipOnlyModal,
	questionModal,
	submitModal
}
