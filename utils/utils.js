import LevelConfig from '@/static/json/level_config.json'
import {
	http,
	serverUrl
} from '../api/api.js'

const getStatus = (key, count = 11) => {
	let statusInfo = uni.getStorageSync("statusInfo")
	if (statusInfo && statusInfo[key] > count) {
		return false
	} else {
		return true
	}
}
const setStatus = key => {
	let statusInfo = uni.getStorageSync("statusInfo") || {}
	if (statusInfo[key]) {
		statusInfo[key]++
	} else {
		statusInfo[key] = 1
	}
	uni.setStorageSync("statusInfo", statusInfo)
}
const getStatusTime = key => {
	let statusInfo = uni.getStorageSync("statusTimeInfo") || {},
		timeNow = new Date().getTime()
	if (!statusInfo || !statusInfo[key]) {
		return true
	} else if ((timeNow - statusInfo[key]) > 24 * 60 * 60 * 1000) {
		return true
	} else {
		return false
	}
}
const setStatusTime = key => {
	let statusInfo = uni.getStorageSync("statusTimeInfo") || {}
	statusInfo[key] = new Date().getTime()
	uni.setStorageSync("statusTimeInfo", statusInfo)
}
const checkLevel = () => {
	let pages = getCurrentPages();
	let page = pages[pages.length - 1].$page.fullPath;
	let level = uni.getStorageSync('userLevel')
	if (!LevelConfig['level_' + level]) {
		return true
	}
	let idx = page.indexOf("?")
	if (idx != -1) {
		page = page.substring(0, idx)
	}
	return LevelConfig['level_' + level].indexOf(page) > -1
}
const isAndroid = () => {
	if (uni.getSystemInfoSync().platform !== 'ios') {
		return true;
	} else {
		var pig = uni.getStorageSync("pig");
		if (!pig) {
			return false;
		}
		return pig === "SZgUSE8bWUSgT71LsRCfWy0TXirCjxcsl0jBaBTygjU";
	}
}
const getPig = () => {
	http("/mp/user/pig", "GET", {}).then(res => {
		if (res.code == 200) {
			uni.setStorageSync("pig", res.data);
		} else {
			uni.setStorageSync("pig", "");
		}
	})
}
module.exports = {
	getStatus,
	setStatus,
	getStatusTime,
	setStatusTime,
	checkLevel,
	isAndroid,
	getPig,
}
