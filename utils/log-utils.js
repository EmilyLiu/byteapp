import {http,serverUrl} from '../api/api.js'

const reportDuration = (source, duration) => {
	var userId = uni.getStorageSync("userId");
	if (!userId) {
		return;
	}
	var params = {
		type: "video-duration",
		userId: userId,
		source: source,
		duration: duration,
	};
	doReport(params);
}

const doReport = (params) => {
	uni.request({
		url: "https://xiaohun.baiziio.com/mp-log/report/videoDuration",
		// url: "http://192.168.3.19:10010/mp-log/report/videoDuration",
		data: params,
		method: "POST",
		success: (resp) => {
			console.log(resp);
		},
		fail: (err) => {
			console.log(err);
		}
	})
}

module.exports = {
	reportDuration,
}
 