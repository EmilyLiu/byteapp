// 格式化数字
const formatNumber = (number, type) => {
	let str = number.toString()
	if (number > 10000) {
		return (Number(number) / 10000).toFixed(type) + '万'
	} else {
		if (number != '-') {
			return Number(number).toFixed(type)
		} else {
			return number
		}
	}
}
const formatIndexValue = (number) => {
	if (Math.ceil(number) == number) {
		return Math.ceil(number)
	}
	return formatNumber(number, 2)
}
// 格式化时间戳
const formatTimeStamp = date => {
	return new Date(date).getTime()
}
// 格式化时间 YYYY-MM-DD
const formatTimeString = date => {
	return `${date.getFullYear()}-${(date.getMonth() + 1).toString()[1] != undefined?date.getMonth() + 1: 0+(date.getMonth() + 1).toString()}-${date.getDate().toString()[1]!=undefined?date.getDate():0+(date.getDate()).toString()}`
	// return [year, month].map(formatNumberString).join('年') + '月'
}
// 格式化时间 YYYY/MM/DD
const formatTopicTime = date => {
	return `${date.getFullYear()}/${(date.getMonth() + 1).toString()[1] != undefined?date.getMonth() + 1: 0+(date.getMonth() + 1).toString()}/${date.getDate().toString()[1]!=undefined?date.getDate():0+(date.getDate()).toString()}`
	// return [year, month].map(formatNumberString).join('年') + '月'
}
// 格式化表格时间
const formatChartTime = date => {
	return `${date.getFullYear()}/${(date.getMonth() + 1).toString()[1] != undefined?date.getMonth() + 1: 0+(date.getMonth() + 1).toString()}/${date.getDate().toString()[1]!=undefined?date.getDate():0+(date.getDate()).toString()}`
	// return [year, month].map(formatNumberString).join('年') + '月'
}
// 格式化时间 天数
const formatDays = (date, type) => {
	let now = new Date()
	// let now = new Date("2020-12-12 17:59:10")
	let time = Math.abs(date - now)
	let days = Math.floor(time / (24 * 3600 * 1000))
	let leave1 = time % (24 * 3600 * 1000)
	let hours = Math.floor(leave1 / (3600 * 1000))
	let leave2 = leave1 % (3600 * 1000)
	let minutes = Math.floor(leave2 / (60 * 1000))
	let leave3 = leave2 % (60 * 1000)
	let seconds = Math.round(leave3 / 1000)
	if (seconds > 0) {
		minutes = minutes + 1
	}
	let returnTime = ''
	if (minutes > 0 && minutes < 60) {
		if (type) {
			returnTime = minutes + "分钟"
		} else {

			returnTime = minutes + "分钟" + returnTime
		}
	} else if (minutes == 60) {
		hours++
	}
	if (hours > 0) {
		if (type) {
			returnTime = hours + "小时"
		} else {
			returnTime = hours + "小时" + returnTime

		}
	}
	if (days > 0) {
		return days + '天'
	} else {
		return returnTime
	}
}
const formatDateString = date => {
	const year = date.getFullYear()
	const month = date.getMonth() + 1
	const day = date.getDate()
	return `${year}年${month}月${day}日`
}
const formatDateTimeString = date => {
	const year = date.getFullYear()
	const month = date.getMonth() + 1
	const day = date.getDate()
	const hour = date.getHours()
	const minute = date.getMinutes()
	return `${year}年${month}月${day}日${hour}时${minute}分`
}
const formatTime = date => {
	const year = date.getFullYear()
	const month = date.getMonth() + 1
	const day = date.getDate()
	const hour = date.getHours()
	const minute = date.getMinutes()
	const second = date.getSeconds()

	return [year, month].map(formatNumberString).join('-')
}
const formatTimeCustom = (date, type = '-', time = false) => {
	const year = date.getFullYear()
	const month = date.getMonth() + 1
	const day = date.getDate()
	const hour = date.getHours()
	const minute = date.getMinutes()
	const second = date.getSeconds()
	return [year, month, day].map(formatNumberString).join(type) + (time ? (' ' + [hour, minute].map(formatNumberString).join(
			':')) :
		'')
}

const formatNumberString = n => {
	n = n.toString()
	return n[1] ? n : n
}
// const formatNumber = n => {
//   n = n.toString()
//   return n[1] ? n : '0' + n
// }
const formatHours = (startTime, endTime, type) => {
	let time = new Date(endTime) - new Date(startTime)
	if (type === 'order') {
		let hours = Math.ceil(time / (3600 * 1000))
		return hours
	} else {
		let days = Math.floor(time / (24 * 3600 * 1000))
		let leave1 = time % (24 * 3600 * 1000)
		let hours = Math.floor(leave1 / (3600 * 1000))
		let leave2 = leave1 % (3600 * 1000)
		let minutes = Math.floor(leave2 / (60 * 1000))
		let leave3 = leave2 % (60 * 1000)
		let seconds = Math.round(leave3 / 1000)
		if (seconds > 0) {
			minutes = minutes + 1
		}
		let returnTime = ''
		if (minutes > 0) {
			returnTime = minutes + "分钟" + returnTime
		}
		if (hours > 0) {
			returnTime = hours + "小时" + returnTime
		}
		if (days > 0) {
			returnTime = days + "天" + returnTime
		}
		return returnTime
	}
}

const formatZero = (num, n) => {
	return (Array(n).join(0) + num).slice(-n);
}
// 获取指定时间毫秒数
const getSecondsFunc = (num) => {
	let date = new Date()
	const time1 = 24 * num * 60 * 60 * 1000
	const hour = date.getHours()
	const minute = date.getMinutes()
	const second = date.getSeconds()
	let time2 = hour * 60 * 60 * 1000 + minute * 60 * 1000 + second * 1000
	return time1 + time2
	// return (Array(n).join(0) + num).slice(-n);
}
// 未读消息时间
const formatMessageTime = (date) => {
	const year = date.getFullYear()
	const month = date.getMonth() + 1 >= 10 ? date.getMonth() + 1 : "0" + (date.getMonth() + 1).toString()
	const day = date.getDate() >= 10 ? date.getDate() : "0" + date.getDate().toString()
	const hour = date.getHours() >= 10 ? date.getHours() : "0" + date.getHours().toString()
	const minute = date.getMinutes() >= 10 ? date.getMinutes() : '0' + date.getMinutes().toString()
	const second = date.getSeconds()
	return `${month}-${day} ${hour}:${minute}`
}
// 获取倒计时
const timerCount = (time) => {
	let days = Math.floor(time / (24 * 3600 * 1000))
	let leave1 = time % (24 * 3600 * 1000)
	let hours = Math.floor(leave1 / (3600 * 1000))
	let leave2 = leave1 % (3600 * 1000)
	let minutes = Math.floor(leave2 / (60 * 1000))
	let leave3 = leave2 % (60 * 1000)
	let seconds = Math.round(leave3 / 1000)
	let timeObj = {
		days: days >= 10 ? days : "0" + days.toString(),
		hours: hours >= 10 ? hours : "0" + hours.toString(),
		minutes: minutes >= 10 ? minutes : "0" + minutes.toString(),
		seconds: seconds >= 10 ? seconds : '0' + seconds.toString()
	}
	return timeObj
}

module.exports = {
	getSecondsFunc,
	formatNumber,
	formatTimeStamp,
	formatDays,
	formatTimeString: formatTimeString,
	formatDateString: formatDateString,
	formatDateTimeString: formatDateTimeString,
	formatTimeCustom: formatTimeCustom,
	formatTime: formatTime,
	formatHours: formatHours,
	formatZero: formatZero,
	formatIndexValue: formatIndexValue,
	formatChartTime,
	formatTopicTime,
	formatMessageTime,
	timerCount
}
