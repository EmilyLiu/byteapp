import {reLogin,vipModal, vipOnlyModal} from "../utils/error.js"
import app from '../App.vue'
// const serverUrl = 'https://xiaohun.baiziio.com'
const serverUrl = 'http://testpayment.baiziio.com'
const getToken = () => {
	return uni.getStorageSync('token')
}
const http = (url, type, params) => {
	let serverUrl1 = app.globalData.isTest?'http://testpayment.baiziio.com':'https://xiaohun.baiziio.com'
	let config = {
		header: {
			token: getToken()
			// token: "38cb9f4d5ecb3c5b2c7bdbf7a8e99e27"
			// token: "4de4edd03d98d8b5cbf2c2e35db34b21"
		},
		url: `${serverUrl1}${url}`,
		method: type,
	}
	if (params) {
		if(type.toUpperCase()=='DELETE') {
			for(let i in params) {
				if(config.url.indexOf('?')<0) {
					config.url += `?${i}=${params[i]}`
				}else {
					config.url += `&${i}=${params[i]}`
				}
			}
		}else {
			config.data = params
		}
		console.log('config.url',config.url)
	}
	return new Promise((resolve, reject) => {
		wx.request({
			...config,
			success: (res) => {
				uni.hideLoading()
				if (res.data.code === 200 ||  res.data.code === 10500  ||  res.data.code === 10501 || res.data.code === 10502 || res.data.code === 10503 || res.data.code=== 10504) {
					resolve(res.data)
				} else if (res.data.code === 401) {
					reLogin()
				} else if (res.data.code === 8888) {
					vipModal(res.data.message)
				} else if (res.data.code === 8889) {
					vipOnlyModal(res.data.message)
				} else if (res.data.code === 8890) {
					vipOnlyModal(res.data.message)
				}else {
					uni.showToast({
						icon: 'none',
						title: res.data.message,
						mask: true,
						duration: 2500
					})
				}
			},
			fail: (err) => {
				uni.hideLoading()
				console.log("请求失败")
				reject(err)
			},
			complete:(result) => {
			}
		})
	}).catch((error) => {})
}
export {
	
	http,
	serverUrl
}
