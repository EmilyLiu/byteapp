import Vue from 'vue'
import App from './App'
import {http,serverUrl} from './api/api.js'
import format from './utils/format.js'
import error from './utils/error.js'
import utils from './utils/utils.js'
import logUtils from './utils/log-utils.js'
import pageDisable from './components/pagedisable.vue'

Vue.component('page-disable',pageDisable)

Vue.config.productionTip = false
Vue.prototype.$http = http
Vue.prototype.$serverUrl = serverUrl
Vue.prototype.$format = format
Vue.prototype.$error = error
Vue.prototype.$utils = utils
Vue.prototype.$logUtils = logUtils
Vue.prototype.$isAndroid = utils.isAndroid
Vue.prototype.$getBackgroundAudioManager = uni.getBackgroundAudioManager()

// Vue.prototype.isIpx = App.globalData.isIpx
// Vue.prototype.navHeight = App.globalData.navHeight

App.mpType = 'app'

const app = new Vue({
    ...App
})
app.$mount()
